import React, { Component } from "react"
import { View } from "react-native"
import { Scene, Router, Stack, Actions } from "react-native-router-flux"

import Splash from './src/Screen/Splash'
import LoginScreen from './src/Screen/loginScreen'
import mainScreen from './src/Screen/mainScreen'
import moviebuffetScreen from './src/Screen/moviebuffetScreen'
import ScanBuffetScreen from './src/Screen/ScanBuffetScreen'
import ScanNormalScreen from './src/Screen/ScanNormalScreen'
export default class Routers extends Component {
    render(){
        return(
            <Router>
                <Stack key='root'>
                    {<Scene key="splash" component={Splash} title="App" hideNavBar/>}
                    {<Scene key="login" component={LoginScreen} hideNavBar/>}
                    {<Scene key="main" component={mainScreen} hideNavBar/>}

                    {<Scene key="moviebuffetScreen" component={moviebuffetScreen} hideNavBar/>}
                    {<Scene key="ScanBuffetScreen" component={ScanBuffetScreen} hideNavBar/>}
                    {<Scene key="ScanNormalScreen" component={ScanNormalScreen} hideNavBar/>}
                </Stack>
            </Router>
        )
    }
}