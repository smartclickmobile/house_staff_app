import { 
    HISTORY,
    HISTORY_ERROR 
} from '../Actions/type'
const INITIAL_STATE = {
   history:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case HISTORY:
        return {
          ...state,
          history: action.payload,
          error: null,
        }
      case HISTORY_ERROR:
        return {
          ...state,
          history: null,
          error: null,
        }
      default:
        return state
    }
  }
  