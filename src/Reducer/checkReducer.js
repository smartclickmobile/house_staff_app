import { 
    CHECKTYPEIPHONE,
    CHECKTYPEIPHONE_ERROR 
} from '../Actions/type'
const INITIAL_STATE = {
   checkIpnoe:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case CHECKTYPEIPHONE:
        return {
          ...state,
          checkIpnoe: action.payload,
          error: null,
        }
      case CHECKTYPEIPHONE_ERROR:
        return {
          ...state,
          checkIpnoe: null,
          error: null,
        }
      default:
        return state
    }
  }
  