import { 
    LOGIN_ERROR,
    LOGIN_SUCCESS 
} from '../Actions/type'
const INITIAL_STATE = {
   login:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case LOGIN_SUCCESS:
        return {
          ...state,
          login: action.payload,
          error: null,
        }
      case LOGIN_ERROR:
        return {
          ...state,
          login: null,
          error: null,
        }
      default:
        return state
    }
  }
  