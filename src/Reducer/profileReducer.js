import { 
    PROFILE_INFO,
    PROFILE_INFO_ERROR 
} from '../Actions/type'
const INITIAL_STATE = {
   profile:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case PROFILE_INFO:
        return {
          ...state,
          profile: action.payload,
          error: null,
        }
      case PROFILE_INFO_ERROR:
        return {
          ...state,
          profile: null,
          error: null,
        }
      default:
        return state
    }
  }
  