import { combineReducers } from "redux"
import checkReducer from './checkReducer'
import authReducer from './authReducer'
import profileReducer from './profileReducer'
import useTicketReducer from './useTicketReducer'
import historyReducer from './historyReducer'
export default combineReducers({
    check: checkReducer,
    auth: authReducer,
    profile: profileReducer,
    used: useTicketReducer,
    history:historyReducer
  })