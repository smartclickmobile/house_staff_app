import { 
    USED_NORMAL,
    USED_NORMAL_ERROR,
    USED_BUFFET,
    USED_BUFFET_ERROR,
    MOVIE_BUFFET,
    MOVIE_BUFFET_ERROR,
    MOVIE_BUFFET_DETAIL,
    MOVIE_BUFFET_DETAIL_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   MovieList:null,
   MovieList_num:null,
   used_normal:null,
   used_normal_num:null,
   used_buffet:null,
   used_buffet_data:null,
   used_buffet_num:null,
   MovieList_detail:null,
   MovieList_detail_num:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case USED_NORMAL:
        return {
          ...state,
          used_normal: true,
          used_normal_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case USED_NORMAL_ERROR:
        return {
          ...state,
          used_normal: false,
          used_normal_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
      case USED_BUFFET:
        return {
          ...state,
          used_buffet: true,
          used_buffet_data: action.payload,
          used_buffet_num:Math.floor((Math.random() * 1000) + 1),
          error: null,
        }
      case USED_BUFFET_ERROR:
        return {
          ...state,
          used_buffet: false,
          used_buffet_data: null,
          used_buffet_num:Math.floor((Math.random() * 1000) + 1),
          error: action.payload,
        }
      case MOVIE_BUFFET:
        return {
            ...state,
            MovieList: action.payload,
            MovieList_num:Math.floor((Math.random() * 1000) + 1),
            error: null,
        }
      case MOVIE_BUFFET_ERROR:
        return {
            ...state,
            MovieList: null,
            error: action.payload,
        }
      case MOVIE_BUFFET_DETAIL:
          return {
              ...state,
              MovieList_detail: action.payload,
              MovieList_detail_num:Math.floor((Math.random() * 1000) + 1),
              error: null,
          }
        case MOVIE_BUFFET_DETAIL_ERROR:
          return {
              ...state,
              MovieList_detail: null,
              error: action.payload,
          }
      default:
        return state
    }
  }
  