import base64 from 'react-native-base64'
export const API_IP = 'https://www.housesamyan.com/uat/houseapi/'
export const CONFIG = {
    headers: {
      "content-type": "multipart/form-data",
      "Authorization": "Basic "+base64.encode("HouseVsSmartClick:")
    },
    timeout: 60000
}