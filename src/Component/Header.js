import React, { Component } from "react"
import {
    Dimensions,
    Text,
    View,
    StyleSheet,
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BG_1, BLACK, BG_2 } from '../constant/color'
import { Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Actions } from "react-native-router-flux"
import I18n from '../../asset/languages/i18n'
import { TEXT_HEADER_EN , TEXT_TH } from '../constant/font'
const dim = Dimensions.get('window');
class header extends Component {
    constructor(props){
        super(props)
        this.state= {
            
        }
    }
    componentWillReceiveProps(nextProps){
        
    }
    render(){
            return(
                <Header 
                    style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'#ddd',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11
                    }} 
                    androidStatusBarColor={BG_2}
                >
                    <Left style={{flex:this.props.title!=undefined?2:0}}>
                        <Button transparent onPress={()=> Actions.pop()}>
                            <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                            <Title style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_HEADER_EN,fontSize:dim.width/100*3}}>{I18n.t('header.back')}</Title>
                        </Button>
                    </Left>
                    <Body style={{flex:9,flexDirection:'row',justifyContent:'center'}}>
                        {this.props.title!=undefined?<Text style={[styles.text_btn_a,{fontSize:dim.width/100*5}]}>{this.props.title}</Text>:<View/>}
                    </Body>
                    <Right style={{flex:this.props.title!=undefined?2:0}}>
                    </Right>
                </Header>
            )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe
    }
  }
  var styles = StyleSheet.create({
    text_btn:{
        textAlign:'center',
        fontSize:dim.width/100*3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:dim.width/100*3,
        color:BLACK,
        fontWeight:'bold',
        fontFamily:I18n.locale=='th'?TEXT_HEADER_EN:TEXT_HEADER_EN
    }
  });

export default connect(mapStateToProps,actions)(header)