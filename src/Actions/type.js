export const CHECKTYPEIPHONE = 'check_type_iphone'
export const CHECKTYPEIPHONE_ERROR = 'check_type_iphone_error'

export const LOGIN_SUCCESS = 'login_success'
export const LOGIN_ERROR = 'login_error'

export const PROFILE_INFO = 'profile_info'
export const PROFILE_INFO_ERROR = 'profile_info_error'

export const HISTORY = 'history'
export const HISTORY_ERROR = 'history_error'

export const USED_NORMAL = 'used_normal'
export const USED_NORMAL_ERROR = 'used_normal_error'
export const USED_BUFFET = 'used_buffet'
export const USED_BUFFET_ERROR = 'used_buffet_error'
export const MOVIE_BUFFET = 'movie_buffet'
export const MOVIE_BUFFET_ERROR = 'movie_buffet_error'
export const MOVIE_BUFFET_DETAIL = 'movie_buffet_detail'
export const MOVIE_BUFFET_DETAIL_ERROR = 'movie_buffet_detail_error'