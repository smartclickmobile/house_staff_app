import axios from "axios"
import { API_IP,CONFIG } from '../constant/constant'
import {
    USED_BUFFET,
    USED_BUFFET_ERROR,
    USED_NORMAL,
    USED_NORMAL_ERROR,
    MOVIE_BUFFET,
    MOVIE_BUFFET_ERROR,
    MOVIE_BUFFET_DETAIL,
    MOVIE_BUFFET_DETAIL_ERROR
} from './type'
import { AsyncStorage, Alert } from 'react-native' 
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"


export const useTicketNormal = option => {
    return async dispatch => {
        const token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("showtimes_seat_id", option.showtimes_seat_id)
        data.append("language", option.language)
       console.warn('useTicketNormal',data)
        axios
            .post(API_IP + "ticket/usednormal",data, CONFIG)
            .then(function (response) {
                console.warn('useTicketNormal',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: USED_NORMAL})
                }else{
                    dispatch({ type: USED_NORMAL_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.warn(error)
            })
    }
}
export const useTicketBuffet = option => {
    return async dispatch => {
        
        const data = new FormData()
        const token = await AsyncStorage.getItem('token')
        data.append("token", token)
        data.append("showtimes_seat_id", option.showtimes_seat_id)
        data.append("language", option.language)
        data.append("device_id", option.device_id)
        
       console.warn('useTicketBuffet',data)
        axios
            .post(API_IP + "ticket/usedbuffet",data, CONFIG)
            .then(function (response) {
                console.warn('useTicketBuffet',response)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: USED_BUFFET ,payload:response.data.data})
                }else{
                    dispatch({ type: USED_BUFFET_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.warn(error)
            })
    }
}
export const getMovieList = option => {
    return async dispatch => {
       
        const data = new FormData()
       
        data.append("language", option.language)
       
        axios
            .post(API_IP + "event/getall",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    var list = []
                    for(var i = 0; i < response.data.data.length; i++){
                        if(response.data.data[i].is_buffet){
                            var temp = {
                                event_id:response.data.data[i].event_id,
                                title:response.data.data[i].title,
                                short_description:response.data.data[i].short_description,
                                description:response.data.data[i].description,
                                thumbnail_web_path:response.data.data[i].thumbnail_web_path,
                                thumbnail_mobile_path:response.data.data[i].thumbnail_mobile_path,
                                is_buffet:response.data.data[i].is_buffet,
                                start_date:response.data.data[i].start_date,
                                end_date:response.data.data[i].end_date,
                                movies:[]
                            }
                            list.push(temp)
                        }
                    }
                    dispatch({ type: MOVIE_BUFFET, payload: list})
                }else{
                    dispatch({ type: MOVIE_BUFFET_ERROR, payload: response.data.message})
                }
            })
            .catch(function (error) {
            //   console.warn(error)
            })
    }
}

export const getMovieList_detail = option => {
    return async dispatch => {
        const token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
        data.append("event_id", option.event_id)
        axios
            .post(API_IP + "event/detail",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    // console.warn('getMovieList_detail',response.data.data)
                    dispatch({ type: MOVIE_BUFFET_DETAIL, payload: response.data.data})
                }else{
                    dispatch({ type: MOVIE_BUFFET_DETAIL_ERROR, payload: response.data.message})
                }
            })
            .catch(function (error) {
            //   console.warn(error)
            })
    }
}