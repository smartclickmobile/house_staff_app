import {CHECKTYPEIPHONE,CHECKTYPEIPHONE_ERROR} from './type'
export const checkiphonex = (data) => {
    return async dispatch => {
        if(data.height==812&&data.width==375){
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'x' })
        }else if(data.height==896&&data.width==414){
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'xr' })
        }else{
            return dispatch({ type: CHECKTYPEIPHONE, payload: 'nox' })
        }
    }
}
export const moneyFormat = (data) => {
    return async dispatch => {
    var decimal = "."
    var thousands = ","
    var decimalCount1 = ''
    
    try {
        // console.warn('moneyFormat',data)
        decimalCount1 = Math.abs(data.decimalCount);
        decimalCount1 = isNaN(data.decimalCount) ? 2 : data.decimalCount;
    
        const negativeSign = data.amount < 0 ? "-" : "";
        let i = parseInt(Math.abs(Number(data.amount) || 0).toFixed(decimalCount1)).toString();
        
        let j = (i.length > 3) ? i.length % 3 : 0;
        var p = negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount1 ? decimal + Math.abs(data.amount - i).toFixed(decimalCount1).slice(2) : "")
        // console.warn('test',data.amount,p)
        return p
      } catch (e) {
        console.log(e)
      }
    }
}
   