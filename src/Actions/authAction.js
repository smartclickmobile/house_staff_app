import axios from "axios"
import { API_IP,CONFIG } from '../constant/constant'
import {
    LOGIN_SUCCESS,
    LOGIN_ERROR,
} from './type'
import { AsyncStorage, Alert } from 'react-native' 
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"
export const login = option => {
    return async dispatch => {
        const data = new FormData()
        data.append("username", option.username)
        data.append("device_id", option.device_id)
        data.append("device_name", option.device_name)
        data.append("platform_id", 2)
        data.append("language", option.language)
        data.append("password", option.password)
        
        console.warn('data',data)
        axios
            .post(API_IP + "staff/login",data, CONFIG)
            .then(function (response) {
                console.warn('login',response)
                if(response.data.code == '0x0000-00000'){
                    AsyncStorage.setItem('token', response.data.data);
                    Actions.main()
                }else{
                    Alert.alert('ERROR',response.data.message)
                }
            })
            .catch(function (error) {
              console.warn(error)
            })
    } 
}
export const logout = option => {
    return async dispatch => {
        console.warn('logout',option)
        const data = new FormData()
        const value = await AsyncStorage.getItem('token')
        data.append("token", value)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "staff/logout",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    AsyncStorage.setItem('token', '');
                    Actions.login()
                }
            })
            .catch(function (error) {
            //   console.warn(error)
            })
    }
}