import axios from "axios"
import { API_IP,CONFIG } from '../constant/constant'
import {
    PROFILE_INFO,
    PROFILE_INFO_ERROR,
} from './type'
import { AsyncStorage, Alert } from 'react-native' 
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"
export const getProfileInfo = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
        const data = new FormData()
        data.append("token", token)
        data.append("language", option.language)
       
        axios
            .post(API_IP + "staff/getinfo",data, CONFIG)
            .then(function (response) {
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: PROFILE_INFO ,payload:response.data.data})
                }else{
                    dispatch({ type: PROFILE_INFO_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
            //   console.warn(error)
            })
    }
}