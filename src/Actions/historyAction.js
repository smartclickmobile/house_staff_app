import axios from "axios"
import { API_IP,CONFIG } from '../constant/constant'
import {
    HISTORY,
    HISTORY_ERROR,
} from './type'
import { AsyncStorage, Alert } from 'react-native' 
import { Actions } from "react-native-router-flux"
import * as actions from "../Actions"
export const getHistory = option => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('token')
       
        const data = new FormData()
        data.append("search_date", option.search_date)
        data.append("token", token)
        data.append("language", option.language)
        console.warn('getHistory',data)
        axios
            .post(API_IP + "ticket/getstaffhistory",data, CONFIG)
            .then(function (response) {
                console.warn('getHistory response',response.data)
                if(response.data.code == '0x0000-00000'){
                    dispatch({ type: HISTORY ,payload:response.data.data})
                }else{
                    dispatch({ type: HISTORY_ERROR ,payload:response.data.data})
                }
            })
            .catch(function (error) {
              console.warn('getHistory error',error)
            })
    }
}