import React, { Component } from "react"
import {
    Dimensions,
    Alert,
    StyleSheet,
    ActivityIndicator
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BLACK } from '../constant/color'
import { Container, View, Content } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH } from '../constant/font'
import QRCodeScanner from 'react-native-qrcode-scanner';
import Header1 from '../Component/Header'
import Sound from 'react-native-sound'
import base64 from 'react-native-base64'
import { RNCamera } from 'react-native-camera'
const dim = Dimensions.get('window');
const beeppress = require('../../asset/sound/beeppress.mp3');
const beepwrong = require('../../asset/sound/beepwrong.mp3');
var whoosh1
var whoosh2
class ScanNormalScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            detail_num:null,
            used_normal_num:null,
            loading:false
        }
    }
    componentDidMount(){
      whoosh1 = new Sound(beeppress, (error) => {
        if (error) {
          console.warn('failed to load the sound', error);
        }else{

        }
      })

      whoosh2 = new Sound(beepwrong, (error) => {
        if (error) {
          console.warn('failed to load the sound', error);
        }else{

        }
      })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.used_normal == true && nextProps.used_normal_num != this.state.used_normal_num){
          this.setState({loading:false})
                    whoosh1.play((success) => {
                        if (success) {
                          console.warn('successfully finished playing');
                        } else {
                          console.warn('playback failed due to audio decoding errors');
                        }
                      });
                      setTimeout(() => {
                        scanner._setScanning(false)
                        }, 2000)
        }
        if(nextProps.used_normal == false && nextProps.used_normal_num != this.state.used_normal_num){
          this.setState({loading:false})
                    whoosh2.play((success) => {
                        if (success) {
                          console.warn('successfully finished playing');
                        } else {
                          console.warn('playback failed due to audio decoding errors');
                        }
                      });
                      setTimeout(() => {
                        scanner._setScanning(false)
                        }, 2000)
        }
    }
    onScan(value){
        
        var data = value.data
        var data_decode = base64.decode(data)
        var data_f = data_decode.replace(/ticket/gi,'')
        var data_sp = data_f.split('-')
        var showtimes_seat_id = data_sp[0]
        var purchase_transaction_id = data_sp[1]
        var temp = {
            purchase_transaction_id:purchase_transaction_id,
            language:I18n.locale
        }
        if(data_sp.length > 1){
          var value = {
              showtimes_seat_id:data_sp[0],
              language:I18n.locale
          }
          this.setState({loading:true})
          this.props.useTicketNormal(value)
        }else{
            Alert.alert('QR CODE',data)
        }
    }
    render(){
        return(
            <Container>
                <Header1 title={'SCAN'}/>
                <Content>
                  {this.state.loading==true &&<View style={{position:'absolute',flex:1,zIndex:98,width:dim.width,height:dim.height/100*80,alignItems:'center',justifyContent:'center',backgroundColor:'#000000',opacity:0.7}}/>}
                  {this.state.loading==true &&<View style={{position:'absolute',flex:1,zIndex:99,width:dim.width,height:dim.height/100*80,alignItems:'center',justifyContent:'center'}}>
                     <ActivityIndicator size="large" color="#ffffff" style={{zIndex:100}}/>
                  </View>}
                  <View style={{width:dim.width,height:dim.height/100*80}}>
                    <QRCodeScanner
                      ref={(camera) => scanner = camera}
                      onRead={(value)=>this.onScan(value)}
                      // cameraStyle={{height:dim.height}}
                      showMarker={true}
                      cameraProps={{zoom:0.5}}
                      // reactivate={true}
                      
                    />
                  </View>
                 
                </Content>
               
               
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,

        used_normal:state.used.used_normal,
        used_normal_num:state.used.used_normal_num,
    }
  }
  var styles = StyleSheet.create({
   
  });
export default connect(mapStateToProps,actions)(ScanNormalScreen)