import React, { Component } from "react"
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    Dimensions,
    Modal,
    StyleSheet,
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2 } from '../constant/color'
import { Container,Form,Item,Input,Label,Icon } from 'native-base';
import DeviceInfo from "react-native-device-info"
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH } from '../constant/font'
const dim = Dimensions.get('window');
class loginScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            username:'',
            password:'',

            username_c:false,
            password_c:false,

            modalError:false,
            textError:''
        }
    }
   async onLogin(){
        if(this.state.username == ''){
            this.setState({modalError:true,textError:I18n.t('login.error.username'),username_c:true})
        }else if(this.state.password == ''){
            this.setState({modalError:true,textError:I18n.t('login.error.password'),username_c:false,password_c:true})
        }else{
            var data = {
                "username":this.state.username,
                "password":this.state.password,
                "device_id":DeviceInfo.getDeviceId(),
                "device_name":DeviceInfo.getDeviceName(),
                "language":I18n.locale
            }
            this.props.login(data)
        }
    }
    render(){
        return(
            <Container>
                <Modal visible={this.state.modalError} transparent={true}
                    onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                    <View  style={{
                    width: "100%",
                    height: "100%",
                    position: "absolute",
                    backgroundColor:'black',
                    opacity:0.7
                    }}/>
                    <View style={{flex:1,justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
                        <View style={{backgroundColor:'#fff',width:dim.width/100*80,borderRadius:dim.width/100*3,alignItems:'center',padding:dim.width/100*5}}>
                            <View style={{borderColor:'#fc5c65',borderWidth:dim.width/100*1,width:dim.width/100*15,height:dim.width/100*15,alignItems:'center',justifyContent:'center',borderRadius:dim.width/100*7.5}}>
                                <Icon name='exclamation' type='FontAwesome5' style={{color:'#fc5c65'}}/>
                            </View>
                            <Text style={styles.text_modal}>{this.state.textError}</Text>
                            <TouchableOpacity style={{borderColor:BG_1,borderWidth:dim.width/100*0.5,padding:dim.width/100*2,backgroundColor:"#ccd1d5",borderRadius:dim.width/100*2,width:dim.width/100*20}} onPress={()=>{this.setState({modalError:false})}}>
                                <Text style={styles.text_modal_btn}>{I18n.t('register.alert.ok')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <LinearGradient colors={[BG_1, BG_2, BG_1]} style={[styles.linearGradient]}>
                    <View style={{alignItems:'center',justifyContent:'flex-end',flex:1}}>
                        <Image
                            source={require('../../asset/Images/logo.png')}
                        />
                    </View>

                    <View style={{flex:3,alignItems:'center',marginTop:dim.height/100*3}}>
                        <Form style={{width:dim.width/100*80}}>
                            <Item floatingLabel>
                                <Label style={styles.label}>{I18n.t('login.username')}</Label>
                                <Input 
                                    ref={(input) => { this.username = input; }} 
                                    style={styles.label}
                                    onChangeText={(value)=>{this.setState({username:value})}}
                                    // onSubmitEditing={()=> this.password.focus()}
                                />
                            </Item>
                            <Item floatingLabel>
                                <Label style={styles.label}>{I18n.t('login.password')}</Label>
                                <Input 
                                    ref={(input) => { this.password = input; }} 
                                    style={styles.label}
                                    onChangeText={(value)=>{this.setState({password:value})}}
                                    secureTextEntry={true}
                                />
                            </Item>
                        </Form>
                        <TouchableOpacity style={[styles.btn]} onPress={()=> {this.onLogin()}}>
                            <Text style={styles.text}>{I18n.t('login.login')}</Text>
                        </TouchableOpacity>
                    </View>
                    
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    btn:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        width:dim.width/100*50,
        backgroundColor:"#ccd1d5",
        paddingVertical:dim.height/100*1.5,
        borderRadius:dim.width/100*1.5,
        borderColor:BG_1,
        borderWidth:1,
        marginTop:dim.height/100*5
    },
    label:{
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text:{
        marginHorizontal:dim.width/100*2,
        fontSize:dim.width/100*3.5,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal:{
        marginVertical:dim.height/100*1.5,
        fontSize:dim.width/100*4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_modal_btn:{
        textAlign:'center',
        fontSize:dim.width/100*4,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    }
  });
export default connect(mapStateToProps,actions)(loginScreen)