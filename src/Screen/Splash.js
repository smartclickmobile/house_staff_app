import React, { Component } from "react"
import {
    View,
    Dimensions,
    AsyncStorage,
    Platform
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"

const dim = Dimensions.get('window');
class Splash extends Component {
    componentDidMount(){
        this.props.checkiphonex(dim)
        setTimeout(() => {
            Actions.login()
        },1000)
    }
    async componentDidMount(){
        const token = await AsyncStorage.getItem('token')
        if(token != undefined && token != null && token != ''){
            if(Platform.OS == 'ios'){
                Actions.main()
            }else{
                setTimeout(() => {
                    Actions.main()
                },1000)
            }
        }else{
            if(Platform.OS == 'ios'){
                Actions.login()
            }else{
                setTimeout(() => {
                    Actions.login()
                },1000)
            }
        }
    }
    render(){
        return(
            <View/>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
    }
  }
export default connect(mapStateToProps,actions)(Splash)