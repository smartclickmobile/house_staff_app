import React, { Component } from "react"
import {
    Dimensions,
    Alert,
    StyleSheet,
    ActivityIndicator,
    View
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BLACK } from '../constant/color'
import { Container, Content } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH } from '../constant/font'
import QRCodeScanner from 'react-native-qrcode-scanner';
import Header1 from '../Component/Header'
const dim = Dimensions.get('window');
import Sound from 'react-native-sound'
import DeviceInfo from "react-native-device-info"
import base64 from 'react-native-base64'
const beeppress = require('../../asset/sound/beeppress.mp3');
const beepwrong = require('../../asset/sound/beepwrong.mp3');
var whoosh1
var whoosh2
class ScanBuffetScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            used_buffet_num:null
        }
    }
    componentDidMount(){
      // console.warn('buffet',this.props.data.show_time)
      whoosh1 = new Sound(beeppress, (error) => {
        if (error) {
          console.warn('failed to load the sound', error);
        }else{

        }
      })

      whoosh2 = new Sound(beepwrong, (error) => {
        if (error) {
          console.warn('failed to load the sound', error);
        }else{

        }
      })
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.used_buffet == true && nextProps.used_buffet_num != this.state.used_buffet_num && nextProps.used_buffet_data!= null){
                    whoosh1.play((success) => {
                        if (success) {
                          console.warn('successfully finished playing');
                        } else {
                          console.warn('playback failed due to audio decoding errors');
                        }
                      });
                      setTimeout(() => {
                        var name = nextProps.used_buffet_data.first_name +' '+ nextProps.used_buffet_data.last_name
                        var id = nextProps.used_buffet_data.member_id
                        Alert.alert('',`member id: ${id}\nname: ${name}`,[
                          {
                            text: 'OK',
                            onPress: () => {
                              scanner._setScanning(false)
                            }
                          }
                        ])
                        
                        }, 2000)
        }
        if(nextProps.used_buffet == false && nextProps.used_buffet_num != this.state.used_buffet_num){
            
                    whoosh2.play((success) => {
                        if (success) {
                          console.warn('successfully finished playing');
                        } else {
                          console.warn('playback failed due to audio decoding errors');
                        }
                      });
                      setTimeout(() => {
                        scanner._setScanning(false)
                        }, 2000)
        }
    }
    onScan(value){
        
        var data = value.data
        var data_decode = base64.decode(data)
        console.warn(data_decode)
        // format: 'ticket'+showtimes_seat_id+'-b'

        var data_f = data_decode.replace(/ticket/gi,'')
        var data_s = data_f.split('-')
        console.warn(data_s)
       
        if(data_s.length > 1){
          if(data_s[1] == 'b'){
            var value = {
              showtimes_seat_id:data_s[0],
              language:I18n.locale,
              device_id:DeviceInfo.getDeviceId(),
            }
            console.warn(value)
            this.props.useTicketBuffet(value)
          }else{
            Alert.alert('QR CODE',data)
          }
        }else{
            Alert.alert('QR CODE',data)
        }
    }
    render(){
        return(
            <Container>
                <Header1 title={'SCAN'}/>
                <Content>
                  {this.state.loading==true &&<View style={{position:'absolute',flex:1,zIndex:98,width:dim.width,height:dim.height/100*80,alignItems:'center',justifyContent:'center',backgroundColor:'#000000',opacity:0.7}}/>}
                  {this.state.loading==true &&<View style={{position:'absolute',flex:1,zIndex:99,width:dim.width,height:dim.height/100*80,alignItems:'center',justifyContent:'center'}}>
                     <ActivityIndicator size="large" color="#ffffff" style={{zIndex:100}}/>
                  </View>}
                  <View style={{width:dim.width,height:dim.height/100*80}}>
                    <QRCodeScanner
                      ref={(camera) => scanner = camera}
                      onRead={(value)=>this.onScan(value)}
                      // cameraStyle={{height:dim.height}}
                      showMarker={true}
                      cameraProps={{zoom:0.5}}
                      // reactivate={true}
                      
                    />
                  </View>
                 
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        used_buffet:state.used.used_buffet,
        used_buffet_num:state.used.used_buffet_num,
        used_buffet_data:state.used.used_buffet_data
    }
  }
  var styles = StyleSheet.create({
    
  });
export default connect(mapStateToProps,actions)(ScanBuffetScreen)