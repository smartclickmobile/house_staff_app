import React, { Component } from "react"
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    Dimensions,
    Alert,
    Modal,
    ActivityIndicator,
    BackHandler,
    StyleSheet,
    AsyncStorage,
    ScrollView
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { Actions } from "react-native-router-flux"
import LinearGradient from 'react-native-linear-gradient';
import { BG_1, BG_2,BLACK } from '../constant/color'
import { Container,Icon,Header,Left,Body,Right,Footer,FooterTab,Button, Content, List,ListItem,Picker } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH, TEXT_HEADER_EN,TEXT_HEADER_EN_BOLD, TEXT_BODY_EN_BOLD } from '../constant/font'
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
const dim = Dimensions.get('window');
class mainScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            index:0,
            profile:null,
            isDateTimePickerVisible:false,
            history_date:moment().format('YYYY-MM-DD'),
            history_list:[],
            history_list_all:[],
            selected_type:0,
            loading:false
        }
    }
    componentDidMount(){
        var data = {
            language:I18n.locale
        }
        this.props.getProfileInfo(data)
        var data_history = {
            language:I18n.locale,
            search_date:this.state.history_date
        }
        this.props.getHistory(data_history)
    }
    componentWillReceiveProps(nextProps){
        console.warn('componentWillReceiveProps',nextProps.profile)
        if(nextProps.profile != null && nextProps.profile != undefined){
            this.setState({profile:nextProps.profile})
        }
        if(nextProps.history_list != null && nextProps.history_list != undefined){
            this.setState({history_list:nextProps.history_list,history_list_all:nextProps.history_list,loading:false})
        }
    }
    onScan(value){
        Alert.alert('QR CODE',value.data)
    }
    onLogout(){
        var data = {
            language:I18n.locale
        }
        this.props.logout(data)
    }
    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
      };
    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
      };
    
      handleDatePicked = date => {
        
        var date_ = moment(date).format('YYYY-MM-DD')
        console.warn("A date has been picked: ", date_);
        this.setState({history_date:date_,isDateTimePickerVisible:false,loading:true},()=>{
            var data_history = {
                language:I18n.locale,
                search_date:this.state.history_date
            }
            this.props.getHistory(data_history)
        })
      };
    _renderScanQR(){
        return(
            <View style={{height:dim.height/100*80,alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity style={{width:dim.width/100*80,borderWidth:dim.width/100*0.5,borderColor:'#333333',padding:dim.width/100*5,borderRadius:dim.width/100*2,alignItems:'center',justifyContent:'center',margin:dim.width/100*5}} onPress={()=> Actions.ScanNormalScreen()}>
                    <Text style={{fontSize:dim.width/100*4,fontFamily:TEXT_HEADER_EN,color:'#333333'}}>SCAN NORMAL TICKET</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{width:dim.width/100*80,borderWidth:dim.width/100*0.5,borderColor:'#333333',padding:dim.width/100*5,borderRadius:dim.width/100*2,alignItems:'center',justifyContent:'center',margin:dim.width/100*5}} onPress={()=> Actions.ScanBuffetScreen()}>
                    <Text style={{fontSize:dim.width/100*4,fontFamily:TEXT_HEADER_EN,color:'#333333'}}>SCAN BUFFET TICKET</Text>
                </TouchableOpacity>
            </View>
        )
    }
    onValueChangeType(value: string) {
        this.setState({
            selected_type: value
        },()=>{
            
           if(value == 0){
                var List = this.state.history_list_all
                this.setState({history_list:List})
           }else if(value == 1){
                var List = []
                for(var i = 0 ; i < this.state.history_list_all.length ; i++){
                    if(this.state.history_list_all[i].is_buffet == false){
                        List.push(this.state.history_list_all[i])
                    }
                }
                this.setState({history_list:List})
           }else if(value == 2){
                var List = []
                for(var i = 0 ; i < this.state.history_list_all.length ; i++){
                    if(this.state.history_list_all[i].is_buffet == true){
                        List.push(this.state.history_list_all[i])
                    }
                }
                this.setState({history_list:List})
            }
        });
    }
    _renderHistory(){
        console.warn(this.state.history_list)
       
        return(
            <View style={{padding:dim.width/100*3}}>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                />
                <TouchableOpacity onPress={()=> this.showDateTimePicker()} style={{borderColor:BG_2,borderWidth:1,flexDirection:'row',padding:dim.height/100*1.2,borderRadius:dim.width/100*1}}>
                    <Text style={{flex:1,fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN}}>{this.state.history_date==''?'Date':this.state.history_date}<Text style={{fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN,color:'red'}}>{this.state.history_date==''?' *':''}</Text></Text>
                    <Icon name='calendar' type='FontAwesome5' style={{color:BG_2,fontSize:dim.width/100*4}}/>
                </TouchableOpacity>
                <Picker
                    placeholder='SELECT TYPE'
                    mode="dropdown"
                    iosHeader='SELECT TYPE'
                    // iosIcon={
                    //     <Icon name='chevron-left' type='FontAwesome5' style={{color:BLACK}}/>
                    // }
                    headerStyle={{fontFamily:TEXT_BODY_EN}}
                    textStyle={{fontFamily:TEXT_BODY_EN}}
                    itemTextStyle={{fontFamily:TEXT_BODY_EN}}
                    placeholderStyle={{fontFamily:TEXT_BODY_EN}}
                    headerTitleStyle={{fontFamily:TEXT_BODY_EN}}
                    headerBackButtonTextStyle={{fontFamily:TEXT_BODY_EN}}
                    style={{ width: '100%' ,marginTop:dim.height/100*1,borderWidth:1,borderColor:BG_2}}
                    selectedValue={this.state.selected_type}
                    onValueChange={this.onValueChangeType.bind(this)}
                >
                    <Picker.Item label='ALL' value={0} />
                    <Picker.Item label='NORMAL' value={1} />
                    <Picker.Item label='BUFFET' value={2} />
                </Picker>
                
                {this.state.loading==true?
                    <View style={{alignItems:'center',justifyContent:'center',marginTop:dim.height/100*5}}>
                        <ActivityIndicator size='large' style={{alignItems:'center',justifyContent:'center',alignSelf:'center'}}/>
                    </View>
                    :
                    <List style={{marginTop:dim.height/100*1}}>
                    {
                        this.state.history_list.map((item,index)=>{
                            return(
                                <View>
                                <ListItem itemDivider style={{backgroundColor:'#565455'}}>
                                    <Text style={{fontFamily:TEXT_BODY_EN_BOLD,color:'#ffffff',fontSize:dim.width/100*4}}>{item.movie_title}</Text>
                                </ListItem>
                                {
                                    item.showtimes.map((item1,index1)=>{
                                        console.warn('item1',item1)
                                            return(
                                                <View>
                                                {
                                                    item1.seats.map((item2,index2)=>{
                                                        return(
                                                            <ListItem>
                                                                <TouchableOpacity style={{flexDirection:'row',flex:1}}>
                                                                    <Left style={{flex:0}}>
                                                                       
                                                                    </Left>
                                                                    <Body style={{justifyContent:'center'}}>
                                                                        <View style={{flexDirection:'row',marginVertical:dim.height/100*0.25}}>
                                                                            <Text style={{flex:4,fontFamily:TEXT_BODY_EN_BOLD,fontSize:dim.width/100*3.5}}>SHOWTIME</Text>
                                                                            <Text style={{flex:6,marginLeft:dim.width/100*3,fontFamily:TEXT_BODY_EN,fontSize:dim.width/100*3.5}}>{item1.start_time}</Text>
                                                                        </View>
                                                                        <View style={{flexDirection:'row',marginVertical:dim.height/100*0.25}}>
                                                                            <Text style={{flex:4,fontFamily:TEXT_BODY_EN_BOLD,fontSize:dim.width/100*3.5}}>SHOWTIME ID</Text>
                                                                            <Text style={{flex:6,marginLeft:dim.width/100*3,fontFamily:TEXT_BODY_EN,fontSize:dim.width/100*3.5}}>{item1.showtimes_id}</Text>
                                                                        </View>
                                                                        <View style={{flexDirection:'row',marginVertical:dim.height/100*0.25}}>
                                                                            <Text style={{flex:4,fontFamily:TEXT_BODY_EN_BOLD,fontSize:dim.width/100*3.5}}>THEATER NAME</Text>
                                                                            <Text style={{flex:6,marginLeft:dim.width/100*3,fontFamily:TEXT_BODY_EN,fontSize:dim.width/100*3.5}}>{item1.theater.name}</Text>
                                                                        </View>
                                                                        
                                                                        <View style={{flexDirection:'row',marginVertical:dim.height/100*0.25}}>
                                                                            <Text style={{flex:4,fontFamily:TEXT_BODY_EN_BOLD,fontSize:dim.width/100*3.5}}>SEAT</Text>
                                                                            <Text style={{flex:6,marginLeft:dim.width/100*3,fontFamily:TEXT_BODY_EN,fontSize:dim.width/100*3.5}}>{`${item2.row}${item2.col_no}`}</Text>
                                                                        </View>
                                                                        <View style={{flexDirection:'row',marginVertical:dim.height/100*0.25}}>
                                                                            <Text style={{flex:4,fontFamily:TEXT_BODY_EN_BOLD,fontSize:dim.width/100*3.5}}>TIME SCAN</Text>
                                                                            <Text style={{flex:6,marginLeft:dim.width/100*3,fontFamily:TEXT_BODY_EN,fontSize:dim.width/100*3.5}}>{item2.check_used_date}</Text>
                                                                        </View>
                                                                        
                                                                    </Body>
                                                                </TouchableOpacity>
                                                            </ListItem>
                                                        )
                                                    })
                                                }
                                                </View>
                                                
                                                
                                            )
                                    })
                                }
                                
                               
                                </View>
                            )
                        })
                    }
                    
                    </List>
                }
               
            </View>
        )
    }
    _renderProfile(){
        var data = this.state.profile
        return(
            <View>
                <Text style={[styles.text_btn_a,{fontSize:dim.width/100*7,marginTop:dim.height/100*1.5,fontFamily:TEXT_HEADER_EN_BOLD}]}>PROFILE</Text>
                <View style={{flexDirection:'row',marginTop:dim.height/100*2,justifyContent:'center'}}>
                    <View style={[styles.Container_row,{flex:0}]}>
                        <Text style={[styles.txt_h]}>USERNAME</Text>
                    </View>
                    <View style={{flex:2.5,padding:dim.width/100*3}}>
                        <Text style={[styles.txt_d]}>{data==null?'':data.username}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',marginTop:dim.height/100*1}}>
                    <View style={[styles.Container_row,{flex:0}]}>
                        <Text style={[styles.txt_h]}>FIRST NAME</Text>
                    </View>
                    <View style={{flex:2.5,padding:dim.width/100*3}}>
                        <Text style={[styles.txt_d]}>{data==null?'':data.first_name}</Text>
                    </View>
                </View>
                <View style={{flexDirection:'row',marginTop:dim.height/100*1}}>
                    <View style={[styles.Container_row,{flex:0}]}>
                        <Text style={[styles.txt_h]}>LAST NAME</Text>
                    </View>
                    <View style={{flex:2.5,padding:dim.width/100*3}}>
                        <Text style={[styles.txt_d]}>{data==null?'':data.last_name}</Text>
                    </View>
                </View>
                
                <View style={{flexDirection:'row',marginTop:dim.height/100*2}}>
                    <TouchableOpacity style={{flex:0,padding:dim.width/100*3}} onPress={()=> {this.onLogout()}}>
                        <Text style={[styles.txt_h]}>SIGN OUT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    render(){
        return(
            <Container>
                <LinearGradient colors={[BG_1, BG_1, BG_2]} style={[styles.linearGradient]}>
                    <Header style={{
                        backgroundColor:BG_1,
                        borderBottomWidth:1,
                        borderBottomColor:'#ddd',
                        paddingTop:dim.height/100*3,
                        height:dim.height/100*11
                    }}
                    androidStatusBarColor={BG_2}
                    >
                        <Left style={{flex:1}}/>
                        <Body style={{flex:1}}>
                            <Image
                                style={{width:dim.width/100*30,height:dim.height/100*5}}
                                source={require('../../asset/Images/logo.png')}
                                resizeMode='contain'
                            />
                        </Body>
                        <Right style={{flex:1}}/>
                    </Header>
                    <Content>
                        {
                            this.state.index == 0 ?
                            this._renderScanQR()
                            :
                            this.state.index == 1?
                            this._renderHistory()
                            :
                            this._renderProfile()

                        }
                    </Content>
                    <Footer style={{backgroundColor:BG_1,height:dim.height/100*10}}>
                        <FooterTab style={{backgroundColor:BG_1,borderTopWidth:this.state.index==0?dim.height/100*0.5:0,borderTopColor:BG_2}}>
                            <Button vertical onPress={()=> this.setState({index:0})}>
                                <Image 
                                source={require('../../asset/Images/icon_home.png')}
                                style={{
                                    width:dim.width/100*7.5,
                                    height:dim.width/100*7.5
                                }}
                                resizeMode='contain'
                                />
                                <Text style={this.state.index==0?styles.text_a:styles.text}>SCAN QR</Text>
                            </Button>
                        </FooterTab>
                        <FooterTab style={{backgroundColor:BG_1,borderTopWidth:this.state.index==1?dim.height/100*0.5:0,borderTopColor:BG_2}}>
                            <Button vertical onPress={()=> 
                                {
                                    var data_history = {
                                        language:I18n.locale,
                                        search_date:this.state.history_date
                                    }
                                    this.props.getHistory(data_history)
                                    this.setState({index:1,loading:true})
                                }
                                
                            }>
                                <Image 
                                source={require('../../asset/Images/time-left.png')}
                                style={{
                                    width:dim.width/100*7.5,
                                    height:dim.width/100*7.5
                                }}
                                resizeMode='contain'
                                />
                                <Text style={this.state.index==1?styles.text_a:styles.text}>HISTORY</Text>
                            </Button>
                        </FooterTab>
                        <FooterTab style={{backgroundColor:BG_1,borderTopWidth:this.state.index==2?dim.height/100*0.5:0,borderTopColor:BG_2}}>
                            <Button vertical onPress={()=> this.setState({index:2})}>
                                <Image 
                                source={require('../../asset/Images/icon_people.png')}
                                style={{
                                    width:dim.width/100*7.5,
                                    height:dim.width/100*7.5
                                }}
                                resizeMode='contain'
                                />
                                <Text style={this.state.index==2?styles.text_a:styles.text}>PROFILE</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        profile:state.profile.profile,
        history_list:state.history.history
    }
  }
  var styles = StyleSheet.create({
    linearGradient: {
      flex: 1
    },
    text_btn:{
        textAlign:'center',
        fontSize:dim.width/100*4.3,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_btn_a:{
        textAlign:'center',
        fontSize:dim.width/100*4.3,
        color:BLACK,
        fontWeight:'bold',
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    Container_row:{
        padding:dim.width/100*3,width:dim.width/100*35
    },
    txt_h:{
        fontSize:dim.width/100*3.5,
        color:BLACK,
        fontWeight:'bold',
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    txt_d:{
        fontSize:dim.width/100*3.5,
        color:BLACK,
        fontFamily:I18n.locale=='th'?TEXT_TH:TEXT_BODY_EN
    },
    text_a:{
        color:BLACK,
        fontFamily:TEXT_HEADER_EN_BOLD,
        fontSize:dim.width/100*2.7,
        marginTop:dim.height/100*1
      },
      text:{
        color:BLACK,
        fontFamily:TEXT_HEADER_EN,
        fontSize:dim.width/100*2.7,
        marginTop:dim.height/100*1
      },
    
  });
export default connect(mapStateToProps,actions)(mainScreen)