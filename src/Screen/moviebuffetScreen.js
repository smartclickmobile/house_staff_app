import React, { Component } from "react"
import {
    Dimensions,
    Alert,
    StyleSheet,
    Image,
    TouchableOpacity,
    ActivityIndicator
  } from "react-native"
import * as actions from "../Actions"
import { connect } from "react-redux"
import { BLACK } from '../constant/color'
import { Container,Content,ListItem,Text,List, View, Left, Body, Right } from 'native-base';
import I18n from '../../asset/languages/i18n'
import { TEXT_BODY_EN, TEXT_TH } from '../constant/font'
import QRCodeScanner from 'react-native-qrcode-scanner';
import Header1 from '../Component/Header'
import { Actions } from "react-native-router-flux";
const dim = Dimensions.get('window');
class moviebuffetScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            movie_list:[],
            check:false,
            loading:true
        }
    }
    componentDidMount(){
        var data = {
            languages:I18n.locale
        }
       this.props.getMovieList(data)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.movieList != null && nextProps.movieList != undefined && this.state.check==false){
            this.setState({movie_list:nextProps.movieList,check:true},()=>{
                
                for(var i = 0 ; i < this.state.movie_list.length; i++){
                    
                    var temp = {
                        languages:I18n.locale,
                        event_id:this.state.movie_list[i].event_id
                    }
                    this.props.getMovieList_detail(temp)
                }
                
            })
            
        }
        if(nextProps.MovieList_detail != null && nextProps.MovieList_detail != undefined){
            var movie_list = this.state.movie_list
            for(var i = 0 ; i < this.state.movie_list.length; i++){
                if(this.state.movie_list[i].event_id == nextProps.MovieList_detail.event_id){
                    var movie = nextProps.MovieList_detail.movies
                    
                    movie_list[i].movies = movie
                }
            }
            this.setState({movie_list:movie_list,loading:false})
        }
    }
    render(){
        
        return(
            <Container>
                <Header1 title={'BUFFET MOVIE'}/>
                <Content>
                {
                    this.state.loading == true ? 
                    <View style={{alignItems:'center',justifyContent:'center',height:dim.height/100*80}}>
                        <ActivityIndicator size="large" color='#333333' />
                    </View>
                    
                    :
                
                <List>
                {
                    this.state.movie_list.map((item,index)=>{
                        // console.warn(item.movies)
                        return(
                            <View>
                                <ListItem key={index} itemDivider>
                                    <Text>{item.title}</Text>
                                </ListItem>
                                {
                                    item.movies.map((item1,index1)=>{
                                        // console.warn(item1.poster_mobile_path)
                                        return(
                                                <ListItem key={index1}>
                                                    <TouchableOpacity style={{flexDirection:'row',flex:1}}  onPress={()=> Actions.ScanBuffetScreen({data:item1})}>
                                                    <Left style={{flex:0}}>
                                                        <Image
                                                            source={{uri:item1.poster_mobile_path}}
                                                            style={{
                                                                width:dim.width/100*20,
                                                                height:dim.width/100*30
                                                            }}
                                                        />
                                                    </Left>
                                                    <Body style={{justifyContent:'center'}}>
                                                        <Text>{item1.title}</Text>
                                                        <Text>{`SHOWTIME: ${item1.start_release_date}`}</Text>
                                                    </Body>
                                                    </TouchableOpacity>
                                                </ListItem>
                                        )
                                    })
                                }
                            </View>
                           
                        )
                    })
                }
                </List>}
                </Content>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIpnoe:state.check.checkIpnoe,
        movieList:state.used.MovieList,
        MovieList_detail:state.used.MovieList_detail
    }
  }
  var styles = StyleSheet.create({
   
  });
export default connect(mapStateToProps,actions)(moviebuffetScreen)