export default {
    
    
    login:{
        error:{
            username:'กรุณากรอกชื่อผู้ใช้',
            password:'กรุณากรอกรหัสผ่าน'
        },
        username:'ชื่อผู้ใช้',
        password:'รหัสผ่าน',
        login:'เข้าสู่ระบบ',
        forget_password:'ลืมรหัสผ่าน'
    },
    header:{
        back:'กลับ'
    },
}